package factorization;

public class Polynomial {

        int[] coefficients;
        int actualdegree;
        
        public Polynomial remainder(Polynomial other) {
               
            Polynomial run = this;
                Polynomial temp;
                int degr,diff;
                
                 //System.out.println("m i here ?");
                 while(run.getActualDegree()>=other.getActualDegree())
                 {
                     //temp=null;
                     degr=run.getActualDegree();
                     //System.out.println("degree of run here is: "+degr);
                     diff=degr-other.getActualDegree();
                     //System.out.println("diff here is"+diff);
                     temp=new Polynomial(degr);
                     for(int i=0;i<=other.getActualDegree();i++)
                     {
                        // System.out.println("hey !!");
                         temp.setCoefficient(i+diff, other.getCoefficient(i));
                     }
                     run=temp.add(run);
                     if(run.getActualDegree()==0 && run.coefficients[0]==0)
                         break;
                     //System.out.println(run.getActualDegree());
                 }

                 return run;
               
            
        }
        
        public Polynomial gcd(Polynomial other)
        {
            
            if(other.getActualDegree()==0)
            {
                return new Polynomial(1,0);
            }
            Polynomial f ,g ,rem ;
            
            if(this.getActualDegree()>=other.getActualDegree())
            {
                //rem=new Polynomial(this.getDegree());
                f=this;
                g=other;
            }
            else 
            {
                //rem=new Polynomial(other.getDegree());
                f=other;
                g=this;
            }
            rem=f.remainder(g);
            
            while(!(rem.getActualDegree()==0 && rem.coefficients[0]==0))
            {
                
                //counter=0;
                f=g;
                g=rem;
                //System.out.println("aaa");
                rem=f.remainder(g);
                
            }
            return g;
         }
        
        public void setCoefficient(int r, int value) {

                if (r < 0) {
                        System.out.println("Invalid degree for setCoefficient");
                        return;
                }
                if (r > getDegree()) 
                { // out of range, must extend the array

                        int newlength;

                        newlength = Math.max(coefficients.length, r + 1);

                        int[] newcoeffs = new int[newlength];

                        for (int i = 0; i < coefficients.length; i++)
                                newcoeffs[i] = coefficients[i];
                        if(value%2==0)           //if multiple of 2,then value is set as 0
                        newcoeffs[r] = 0;
                        else
                        newcoeffs[r] = value%2;    //else set as value%2
                        coefficients = newcoeffs;
                } 
                else
                {
                        if(value%2==0)
                            coefficients[r] = 0;
                        else
                            coefficients[r] = value%2;
                }

                updateDegree(); 

        }
        
        public int getCoefficient(int r) {

                if (r > getDegree() || r < 0)
                        return 0;

                return coefficients[r];

        }

        
        

        public double evaluate(double x) {

                double sum = 0;

                for (int i = 0; i < coefficients.length; i++)
                        sum += (double) coefficients[i] * Math.pow(x, i);

                return sum;

        }

        public Polynomial evaluate(Polynomial other) {

                Polynomial output;

                output = new Polynomial(other.getDegree() * this.getDegree());

                for (int a = 0; a <= other.getActualDegree(); a++)
                        output = output.add(this.powerOf(a).scalarmul(
                                        other.getCoefficient(a)));

                return output;

        }

        public Polynomial clonePoly() {

                return new Polynomial(coefficients);

        }

        public Polynomial add(Polynomial other) {

                Polynomial large, small, output;
                
                if (other.getActualDegree() > getActualDegree()) {
                        large = other;
                        small = this;
                } else {
                        large = this;
                        small = other;
                }

                output = large.clonePoly();

                for (int i = 0; i <= small.getActualDegree(); i++)
                        output.setCoefficient(i, output.getCoefficient(i)
                                        + small.getCoefficient(i));
                
                return output;

        }

        public Polynomial subtract(Polynomial other) {

                return this.add(other);

        }

        
        public Polynomial multiply(Polynomial other) {

                Polynomial output;

                output = new Polynomial(other.getActualDegree() + this.getActualDegree());

                for (int a = 0; a <= this.getActualDegree(); a++)
                        for (int b = 0; b <= other.getActualDegree(); b++)
                                output.setCoefficient(a + b, output.getCoefficient(a + b)
                                                + other.getCoefficient(b) * this.getCoefficient(a));

                return output;

        }

        public Polynomial scalarmul(int k) {

                Polynomial output;

                output = new Polynomial(getDegree());

                for (int i = 0; i <= getActualDegree(); i++)
                        output.setCoefficient(i, k * getCoefficient(i));

                return output;

        }

        public Polynomial powerOf(int pow) {

                if (pow < 0) {
                        System.out.println("Negative powers are not allowed");
                        return null;
                }

                int[] one = { 1 };
                Polynomial output = new Polynomial(one); // this takes care of pow==0
                                                                                                        // case

                for (int i = 1; i <= pow; i++)
                        output = output.multiply(this);

                return output;
        }
        
        public Polynomial derivative() {

                Polynomial output = new Polynomial(this.getActualDegree() - 1);

                for (int i = 1; i <= getActualDegree(); i++)
                        output.setCoefficient(i - 1, this.getCoefficient(i) * i);

                return output;

        }
        
        public boolean equals(Polynomial other)
        {
            int[] temp1,temp2;
            temp1=this.coefficients;
            temp2=other.coefficients;
            boolean flag=true;
            if(this.getActualDegree()==other.getActualDegree())
            {
                for(int i=0;i<this.getActualDegree();i++)
                {
                    if(temp1[i]!=temp2[i])
                    {
                        flag = false;
                        break;
                    }
                }
                return flag;
            }else return false;
        }

    public String toString() {
        if (this.getDegree() ==  0) return "" + coefficients[0];
        if (this.getDegree() ==  1) return coefficients[1] + "x " + ((coefficients[0] > 0) ? "+ " + coefficients[0] : (coefficients[0] == 0) ? "" : "- " + -coefficients[0]);
        String s = coefficients[this.getDegree()] > 0 ? coefficients[this.getDegree()] + "x^" + this.getDegree() : "";
        for (int i = this.getDegree()-1; i >= 0; i--) {
            if      (coefficients[i] == 0) continue;
            else if (coefficients[i]  > 0) s = s.equals("") ? s + ( coefficients[i]) : s + " + " + ( coefficients[i]);
            else if (coefficients[i]  < 0) s = s.equals("") ? s + ( coefficients[i]) : s + " - " + (-coefficients[i]);
            if      (i == 1) s = s + "x";
            else if (i >  1) s = s + "x^" + i;
        }
        return s;
    }
    
    public Polynomial(int a, int b) {

                this(b);
                this.setCoefficient(b, a);
                
                updateDegree(); 

        }
        
        public Polynomial(int deg) {

                if (deg < 0) {
                        System.out.println("Polynomial degree must not be negative");
                        System.exit(1);
                }

                coefficients = new int[deg + 1];

                updateDegree(); 

        }

        public Polynomial(int[] coeffs) {

                coefficients = (int[]) coeffs.clone();

                updateDegree(); 

        }
        
        public int getDegree() {

                return coefficients.length - 1;

        }

        
        public int getActualDegree() {

                return actualdegree;

        }

        
        private void updateDegree() {

                for (int i = coefficients.length - 1; i >= 0; i--)
                        if (coefficients[i] != 0) {
                                actualdegree = i;
                                return;
                        }

                actualdegree = 0; 
                
        }
}