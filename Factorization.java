package factorization;

import static java.lang.Math.pow;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Factorization {

    public int[] decToBin(int n)
    {
        int[] binary = new int[25];
        int index = 0;
        while(n>0)
        {
            binary[index++]=n%2;
            n=n/2;
        }
        return binary;
    }
    
    public ArrayList<Polynomial> Berlekamp(Polynomial f)
    {
        
        //System.out.println("dere?");
        Polynomial der = f.derivative();
        //System.out.println(f.toString());
        Polynomial gcd = f.gcd(der);
        //System.out.println(gcd.toString());
        Polynomial temp,temp1;
        int[] bin,tempc,binc;
        int counter=0;
        ArrayList<Polynomial> p = new ArrayList<Polynomial>();
        ArrayList<Polynomial> basis = new ArrayList<Polynomial>();
        ArrayList<Polynomial> irr = new ArrayList<Polynomial>();
        int[][] coeffs;
        
        int deg = der.getActualDegree();
        int fdeg=f.getActualDegree();
        binc = new int[fdeg+1];
        coeffs= new int[fdeg+1][fdeg+1];
        int degree = gcd.getActualDegree();
        int[] coeff = gcd.coefficients;
        boolean flag =false;
        boolean flag1=false;
        //System.out.println("degree is: "+degree+" and coeff[0] is: "+coeff[0]);
        if(degree==0 && coeff[0]==1)
        {
            coeff=new int[fdeg+1];
            //System.out.println("hey dere");
            for(int i=0;i<fdeg;i++)
            {
                temp= new Polynomial(1,2*i);
                //System.out.println("remainder here is :" +temp.remainder(f) );
                p.add(temp.remainder(f));
            }
            
            for(int i=0 ; i<fdeg ;i++)
            {
                temp=p.get(i);
                //System.out.println("temp is: "+temp.toString());
                coeff=temp.coefficients;
                if(((coeff[i]+1)%2)==0)
                    coeff[i]=0;
                else
                    coeff[i]=1;
                temp=new Polynomial(coeff);
                //System.out.println("new temp is and its coeff length is: "+temp.toString()+"\t"+temp.coefficients.length);
                p.set(i, temp);
            }
            //System.out.println(fdeg);
            int[] finalc = new int[fdeg];
            int xyz=0;
            for(int i=0;i<fdeg;i++)
            {
                
                temp=p.get(i);
                //System.out.println("temp is: "+temp.toString());
                xyz=temp.getActualDegree()+1;
                coeff=temp.coefficients;
                for(int j=0;j<xyz;j++)
                {
                    finalc[j]=coeff[j];
                    //System.out.print(finalc[j]);
                }
                for(int j=xyz;j<fdeg;j++)
                {
                    finalc[j]=0;
                    //System.out.print(finalc[j]);
                }
                //System.out.println("\n");
                temp=new Polynomial(finalc);
               // System.out.println("another new temps are and their coeff length are : " + temp.toString()+"\t"+temp.coefficients.length);
                p.set(i, temp);
            }
            
            //System.out.println(p.get(0).coefficients[0]);
            for(int i=1 ; i<pow(2,fdeg) ; i++)
            {
                bin = decToBin(i);
                
                for(int j=0;j<fdeg;j++)
                {
                    for(int k=0;k<fdeg;k++)
                    {
                        tempc=p.get(k).coefficients;
                       // System.out.println("value of tempc "+j+" is: "+tempc[j]+" and value of bin "+k+"is : "+bin[k]);
                        //System.out.println("coeff lenght here is:"+tempc.length);
                        coeffs[k][j]=tempc[j]*bin[k];
                    }
                    
                    tempc=new int[fdeg];
                    
                    for(int k=0;k<fdeg;k++)
                    {
                        tempc[j]=coeffs[k][j]+tempc[j];
                        
                    }
                   // System.out.println("sum of coeffs here is: "+tempc[j]);
                    if(tempc[j]%2!=0)
                    {
                        flag=true;
                        break;
                    }
                    else
                        flag=false;
                }
                if(flag==false)
                {
                    temp=new Polynomial(bin);
                    for(int a=0;a<basis.size();a++)
                    {
                        temp1=basis.get(a);
                        coeff=temp.coefficients;
                        for(int b=0;b<basis.size();b++)
                        {
                           
                           if(Arrays.equals(basis.get(b).add(temp1).coefficients,coeff ))    
                           {
                               flag1=true ;
                               break;
                           }
                           else
                               flag1=false;
                        }
                        if(flag1==true)
                            break;
                    }
                    if(flag1==true)
                   ;// System.out.println("NOT ADDED: "+temp.toString());
                    else
                    {
                     //   System.out.println("ADDED: "+temp.toString());
                        basis.add(temp);
                    }
                    
                }
            }
            
            irr.add(basis.get(0));
            
            
            for(int i=1;i<basis.size();i++)
            {
                temp=basis.get(i);
                //System.out.println("temp is: "+temp.toString());
                tempc=temp.coefficients;
                temp= f.gcd(temp);
                //System.out.println("Irreducible poly is: "+temp.toString());
                flag = false;
                for(int j=0;j<irr.size();j++)
                {
                   // System.out.println("Almost done!!:P");
                    if(irr.get(j).equals(temp))
                    {
                        flag=true;
                        break;
                    }
                    else
                        flag=false;
                }
                if(flag==false)
                    irr.add(temp);
                
                temp=basis.get(i);
                tempc=temp.coefficients;
                if(tempc[0]==1)
                    temp.setCoefficient(0,0);
                else if(tempc[0]==0)
                    temp.setCoefficient(0,1);
                temp=f.gcd(temp);
                //System.out.println("Irreducible poly is: "+temp.toString());
                flag=false;
                for(int j=0;j<irr.size();j++)
                {
                    //System.out.println("irr.get(j) is: "+irr.get(j).toString());
                    if(irr.get(j).equals(temp))
                    {
                       // System.out.println("Am I here?");
                        flag=true;
                        break;
                    }
                    else
                        flag=false;
                }
                if(flag==false)
                    irr.add(temp);
            }
            
        }
        return irr;
    }
    
    public static void main(String[] args)
    {
        Factorization z = new Factorization();
        Scanner stdin = new Scanner(System.in);
        boolean flag=false;
        System.out.println("Enter the degree of the polynomial u want to enter:\n");
        int degree = stdin.nextInt();
        int[] coeff = new int[degree+1];
        do{
            System.out.println("Enter the value of constant term");
            coeff[0]=stdin.nextInt();
            if(coeff[0]/2>0)
            {
                System.out.println("Please Enter values in Z2 only i.e. only 0 or 1. Try Again !!");
                flag=true;
            }
            else
                flag=false;
        }while(flag==true);
        flag=false;
        for(int i=1;i<degree+1;i++)
        {
            do{
                System.out.println("Enter the coefficient of x^"+i);
                coeff[i]=stdin.nextInt();
                if(coeff[i]/2>0)
                {
                    System.out.println("Please Enter values in Z2 only i.e. only 0 or 1. Try Again !!");
                    flag=true;
                }
                else flag=false;
            }while(flag==true);
        }
        Polynomial p = new Polynomial(coeff);
        System.out.println("The Polynomial you entered is: "+p.toString());
        ArrayList<Polynomial> irr = z.Berlekamp(p);
        System.out.println("The irreducible factors of "+p.toString()+" are : \n");
        for(int i=0;i<irr.size();i++)
        {
            System.out.println(irr.get(i).toString());
        }
                
    }
    
}
